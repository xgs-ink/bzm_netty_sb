package com.fjb.service.im;

import com.fjb.entity.JsonResult;
import com.fjb.pojo.im.ImChatFriendInfo;
import com.fjb.pojo.im.ImChatFriendRequestLogs;
import com.fjb.pojo.im.vo.ImChatFriendRequestLogsVo;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 好友请求记录 服务类
 * </p>
 *
 * @author hemiao
 * @since 2020-06-02
 */
public interface ImChatFriendRequestLogsService extends IService<ImChatFriendRequestLogs> {
	
	/**
	 * @Description:查询好友请求记录
	 * @param userId
	 * @return
	 * List<ImChatFriendRequestLogsVo>
	 * @exception:
	 * @author: hemiao
	 * @time:2020年6月3日 下午7:29:34
	 */
	List<ImChatFriendRequestLogsVo> selectRequestLogsList(Integer userId);
	
	/**
	 * @Description:更新好友申请通过
	 * @param updateLogs
	 * @param fi
	 * @return
	 * JsonResult<Boolean>
	 * @exception:
	 * @author: hemiao
	 * @time:2020年6月4日 下午9:49:32
	 */
	JsonResult<Boolean> updateRequestStatus1(ImChatFriendRequestLogs updateLogs, ImChatFriendInfo fi);

}
