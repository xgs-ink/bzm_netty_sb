package com.fjb.service.user.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fjb.mapper.user.SysUserMapper;
import com.fjb.pojo.user.SysUser;
import com.fjb.service.user.SysUserService;

/**
 * <p>
 * 用户信息 服务实现类
 * </p>
 *
 * @author hemiao
 * @since 2019-12-22
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {

}
