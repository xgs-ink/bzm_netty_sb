package com.fjb.tool.nio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.util.Set;

public class NioDemo {
	
	public static void main(String[] args) {
		
		try {
			ServerSocketChannel socketChannel = ServerSocketChannel.open();
			socketChannel.configureBlocking(false);
			socketChannel.socket().bind(new InetSocketAddress(8088));
			Selector selector = Selector.open();
			socketChannel.register(selector,SelectionKey.OP_ACCEPT);
			Set<SelectionKey> selectedKeys = selector.selectedKeys();
			for (SelectionKey sKey : selectedKeys) {
				
				boolean acceptable = sKey.isAcceptable();
				
				boolean connectable = sKey.isConnectable();
				
				boolean readable = sKey.isReadable();
				
				
				
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
