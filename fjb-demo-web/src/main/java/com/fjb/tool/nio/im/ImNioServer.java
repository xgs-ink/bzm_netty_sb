package com.fjb.tool.nio.im;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.util.Set;

public class ImNioServer {
	
	public void start() {
		try {
			// 创建 nio 通道
			ServerSocketChannel socketChannel = ServerSocketChannel.open();
			// 切换成非组设模式
			socketChannel.configureBlocking(false);
			// 绑定服务端口
			socketChannel.socket().bind(new InetSocketAddress(8088));
			// 创建 选择器
			Selector selector = Selector.open();
			// 注册接受事件	
			// NIO SelectionKey中定义的4种事件
			// 1、SelectionKey.OP_ACCEPT —— 接收连接继续事件，表示服务器监听到了客户连接，服务器可以接收这个连接了
			// 2、SelectionKey.OP_CONNECT —— 连接就绪事件，表示客户与服务器的连接已经建立成功
			// 3、SelectionKey.OP_READ —— 读就绪事件，表示通道中已经有了可读的数据，可以执行读操作了（通道目前有数据，可以进行读操作了）
			// 4、SelectionKey.OP_WRITE —— 写就绪事件，表示已经可以向通道写数据了（通道目前可以用于写操作）
			//  这里 注意，下面两种，SelectionKey.OP_READ ，SelectionKey.OP_WRITE ，
			// 1.当向通道中注册SelectionKey.OP_READ事件后，如果客户端有向缓存中write数据，下次轮询时，则会 isReadable()=true；
			// 2.当向通道中注册SelectionKey.OP_WRITE事件后，这时你会发现当前轮询线程中isWritable()一直为ture，如果不设置为其他事件
			socketChannel.register(selector, SelectionKey.OP_ACCEPT);
			while(selector.select()>0) {
				// 获取就绪事件
				Set<SelectionKey> selectedKeys = selector.selectedKeys();
				for (SelectionKey sKey : selectedKeys) {
					//如果是客户端连接事件
					
						
				}
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		
		
		
	}
	
}
