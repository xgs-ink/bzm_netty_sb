package com.fjb.tool.im.service.handler;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

/**
 * @Description:入站处理程序适配器
 * @author hemiao
 * @time:2020年4月3日 下午2:00:03
 */
public class MyServerInboundHandler extends ChannelInboundHandlerAdapter{

	/**
	 * 注册通道
	 */
	@Override
	public void channelRegistered(ChannelHandlerContext ctx) throws Exception {
		System.out.println("Inbound channelRegistered 注册通道");
	}
	
	/**
	 * 通道未注册
	 */
	@Override
	public void channelUnregistered(ChannelHandlerContext ctx) throws Exception {
		System.out.println("Inbound channelUnregistered 通道未注册");
	}
	
	/**
	 * 通道激活时触发，当客户端connect成功后，服务端就会接收到这个事件，从而可以把客户端的Channel记录下来，供后面复用
	 */
	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		System.out.println("Inbound channelActive 通道激活时触发");
	}
	
	/**
	 * 频道不活跃
	 */
	@Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception {
		System.out.println("Inbound channelInactive 频道不活跃");
	}
	
	/**
	 * 这个必须用啊，当收到对方发来的数据后，就会触发，参数msg就是发来的信息，可以是基础类型，也可以是序列化的复杂对象。
	 */
	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
		System.out.println("Inbound channelRead 当收到对方发来的数据后，就会触发，参数msg就是发来的信息，可以是基础类型，也可以是序列化的复杂对象");
	}
	
	/**
	 * channelRead执行后触发
	 */
	@Override
	public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
		System.out.println("Inbound channelReadComplete channelRead执行后触发");
	}
	
	/**
	 * 用户事件触发
	 */
	@Override
	public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
		System.out.println("Inbound userEventTriggered 用户事件触发");
	}
	
	/**
	 * 通道可写性改变
	 */
	@Override
	public void channelWritabilityChanged(ChannelHandlerContext ctx) throws Exception {
		System.out.println("Inbound channelWritabilityChanged 通道可写性改变");
	}

	/**
	 * 出错是会触发，做一些错误处理
	 */
	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		System.out.println("Inbound exceptionCaught 出错是会触发，做一些错误处理");
	}
}
