package com.fjb.tool.future;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class FutureDemo {
	
	private ExecutorService executor = Executors.newSingleThreadExecutor();
     
    public Future<Integer> calculate(Integer input) {        
        return executor.submit(() -> {
            Thread.sleep(1000);
            return input * input;
        });
    }
	
	public static void main(String[] args) {
		FutureDemo fDemo  = new FutureDemo();
		Future<Integer> calculate = fDemo.calculate(123456);
		System.out.println(calculate.toString());
			
	}
	
}
