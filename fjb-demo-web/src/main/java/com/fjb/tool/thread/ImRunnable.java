package com.fjb.tool.thread;

/**
 * @Description:TODO
 * @author hemiao
 * @time:2020年4月20日 下午3:35:55
 */
public class ImRunnable implements Runnable{

	@Override
	public void run() {
		Thread currentThread = Thread.currentThread();
		String name = currentThread.getName();
		System.out.println(name+" 限制开始 ");
		System.out.println(" isInterrupted  sleep前 ："+currentThread.isInterrupted());
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println(" isInterrupted   sleep前 ："+currentThread.isInterrupted());
	}
	
	public static void main(String[] args) {
		
		ImRunnable imRunnable = new ImRunnable();
		Thread thread = new Thread(imRunnable);
		thread.setName("123456-hm");
		thread.start();
		try {
			Thread.sleep(1000);
			thread.interrupt();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
