package com.fjb.entity;

public class JsonPagesResult<T> {

	/**
	 * 返回数据
	 */
    private T dataObject;
    
    /**
     * 错误码
     */
    private Integer code;
    
    /**
     * 描述信息
     */
    private String msg;
    
    /**
     * 总数量
     */
    private Long total;
    
    /**
     * 总页数
     */
    private int pages;
    
    public JsonPagesResult(){
    	
    }
    
    public JsonPagesResult(T dataObject,HttpCode httpCode,Long total,int pages) {
        this.dataObject = dataObject;
        this.code = httpCode.getCode();
        this.msg = httpCode.getMsg();
        this.total=total;
        this.pages=pages;
    }

    public T getDataObject() {
        return dataObject;
    }

    public void setDataObject(T dataObject) {
        this.dataObject = dataObject;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

	public int getPages() {
		return pages;
	}

	public void setPages(int pages) {
		this.pages = pages;
	}
    
}
