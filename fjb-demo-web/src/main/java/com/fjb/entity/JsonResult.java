package com.fjb.entity;

import java.io.Serializable;

public class JsonResult<T> implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 8686627581058827045L;
	/**
	 * 返回数据
	 */
    private T data;
    
    /**
     * 错误码
     */
    private Integer code;
    
    /**
     * 描述信息
     */
    private String msg;
    
    public JsonResult(){
    		
    }
    
    public JsonResult(T data,HttpCode httpCode) {
        this.data = data;
        this.code = httpCode.getCode();
        this.msg = httpCode.getMsg();
    }

    public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}
