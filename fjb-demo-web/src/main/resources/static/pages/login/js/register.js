$(document).keypress(function(e) {
   var eCode = e.keyCode ? e.keyCode : e.which ? e.which : e.charCode;
    if (eCode == 13){
    	initRegister();
    }
});

// 注册	
document.getElementsByClassName('register-btn')[0].onclick = function () {
	initRegister();
}

// 注册
function initRegister() {
	$(".error_msg").empty().append('');	  
	var phone = document.getElementById("phone").value;
	var password = document.getElementById("password").value;	
	var nickname = document.getElementById("nickname").value;	
	//var code = document.getElementById("code").value;	
	// var registerType = $("input[name='registerType']:checked").val();
	/*if(!registerType){
		initErrorMsg("注册类型不能为空");
		return false;
	}*/	
	if(!phone){
		initErrorMsg("手机号不能为空");
		return false;	
	}
	if(!password){
		initErrorMsg("密码不能为空");
		return false;
	}			
	if(!nickname){
		initErrorMsg("昵称不能为空");
		return false;
	}
	var gender = $("input[name='gender']:checked").val();
	if(!gender){
		initErrorMsg("性别不能为空");
		return false;
	}
	/*if(!code){
		initErrorMsg("验证码不能为空");
		return false;
	}*/
	
	var dataQuery = {
		phone:phone,
		password:password,
		nickname:nickname,
		//code:code,
		//registerType,registerType,
		gender:gender
	}
	$.ajax({													
		url:'/register/addInfo', // 请求的url地址	
		dataType:'json', 	// 返回格式为json	
		data:dataQuery,												
		type:'post', 		// 请求方式		
		success: function(res) {
			if(res.code==200){	
				window.location.href = "/login";
			}else{	
				initErrorMsg(res.msg);
			}
		},			
		error: function() {
			
		} 		
	});		
}

// 登陆 	
document.getElementsByClassName('login-btn')[0].onclick = function () {
	window.location.href = "/login";	
}

// 错误消息
function initErrorMsg(msg) {
	$(".error_msg").empty().append('<p class="error_msg_context">'+msg+'</p>');  
}

//忘记密码	forget-password-btn
$(document).on('click', '.forget-password-btn', function () {
	var html = ""; 	
	html += '<p class="error_msg_context">功能待开发中,请联系管理员 wx </p>';	
	html += '<img alt="" src="./../static/images/bzm-wx.jpg" style="width: 100px;margin-top: 5px;">';
	$(".error_msg").empty().append(html);	
});


